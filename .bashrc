LS_COLORS=$LS_COLORS:'di=0;35:' ; export LS_COLORS

function frameworkpython {
    if [[ ! -z "$VIRTUAL_ENV" ]]; then
        PYTHONHOME=$VIRTUAL_ENV /Library/Frameworks/Python.framework/Versions/3.5/bin/python3 "$@"
    else
        /usr/local/bin/python "$@"
    fi
}
