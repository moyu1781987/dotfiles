execute pathogen#infect()

filetype plugin indent on
syntax on

source /usr/local/lib/python2.7/site-packages/powerline/bindings/vim/plugin/powerline.vim
set nocompatible              " be iMproved, required
filetype off                  " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'The-NERD-tree'
Plugin 'Valloric/YouCompleteMe'
Plugin 'crusoexia/vim-monokai'
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

colorscheme monokai
set guifont=Meslo\ LG\ M\ DZ\ Regular\ for\ Powerline:h14
set number
set hlsearch
set backspace=2
nnoremap j jzz
nnoremap k kzz
set tabstop=4
set laststatus=2
call pathogen#infect()
map <C-n> :NERDTreeToggle<CR>

nnoremap th  :tabfirst<CR>
nnoremap tj  :tabnext<CR>
nnoremap tk  :tabprev<CR>
nnoremap tl  :tablast<CR>
nnoremap tt  :tabedit<Space>
nnoremap tn  :tabnext<Space>
nnoremap tm  :tabm<Space>
nnoremap td  :tabclose<CR>
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
