eval "$(rbenv init -)"

# The orginal version is saved in .bash_profile.pysave
if [ -f "$(brew --prefix)/opt/bash-git-prompt/share/gitprompt.sh" ]; 
then
    source "$(brew --prefix)/opt/bash-git-prompt/share/gitprompt.sh"
fi
export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacad
export PATH="$PYENV_ROOT/bin:$PATH"
if which pyenv > /dev/null; then eval "$(pyenv init -)"; fi
export WORKON_HOME=$HOME/.virtualenvs
alias la="ls -al" 
export PATH="$PATH:/Applications/MATLAB_R2015b.app/bin/"

